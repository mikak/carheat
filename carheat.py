# -*- coding: utf-8 -*-
"""
Created on Sat Nov 12 16:25:42 2016

@author: mikak
"""

import datetime
from time import sleep

import weather
import switch

latest_handled_time = None
test_mode = True
sim_time = datetime.datetime.now()

schedule = {
    'Mon-Fri': '7:30'
}

weather_service = weather.WeatherFmi()


class ScheduleDaily:
    def __init__(self):
        self.daily_time = datetime.time(7, 30)

    def next_time(self, t0):
        if (t0.time() > self.daily_time):
            t0 = t0 + datetime.timedelta(days=1)
        return datetime.datetime.combine(t0.date(), self.daily_time)


# in minutes
def heat_time(temperature):
    MIN_HEAT_TIME = 30
    MAX_HEAT_TIME = 180
    MIN_HEAT_TIME_TEMP = 5.0
    MAX_HEAT_TIME_TEMP = -20.0

    # unknown temperature
    if temperature is None:
        # could use statistics by month/day
        temperature = -5.0

    if temperature > MIN_HEAT_TIME_TEMP:
        return 0
    elif temperature < MAX_HEAT_TIME_TEMP:
        return MAX_HEAT_TIME
    else:
        return (1 + (MAX_HEAT_TIME_TEMP - temperature) /
            (MIN_HEAT_TIME_TEMP - MAX_HEAT_TIME_TEMP)) * \
            (MAX_HEAT_TIME - MIN_HEAT_TIME) + MIN_HEAT_TIME


def next_departure(schedule, onlynew=True):
    global latest_handled_time
    curr_time = current_time()
    if latest_handled_time is None or curr_time > latest_handled_time:
        latest_handled_time = curr_time
    next_time = schedule.next_time(curr_time)
    if onlynew:
        if next_time > latest_handled_time:
            return next_time
    elif next_time > curr_time:
        return next_time
    return None

def current_temperature():
    return weather_service.get_temperature()

def current_time():
    if test_mode:
        global sim_time
        sim_time += datetime.timedelta(minutes=5)
        return sim_time
    else:
        return datetime.datetime.now()

def wait(seconds):
    if test_mode:
        sleep(0.02)
    else:
        sleep(seconds)


class IdleState:
    name = 'IDLE'
    def __init__(self):
        return

    def enter(self):
        return

    def do_work(self, t, switch):
        global latest_handled_time
        temperature = current_temperature()
        next_time = next_departure(schedule)
        if next_time is not None and (t + datetime.timedelta(minutes=heat_time(temperature))) >= next_time:
            print("--", next_time, latest_handled_time)
            if switch.set_state(True):
                # update handled times to next_dept
                latest_handled_time = next_time
                return ('HEAT', 0)
        return (self.name, 120)

class HeatState:
    name = 'HEAT'
    def __init__(self):
        self.my_departure = None
        return

    def enter(self):
        self.my_departure = latest_handled_time

    def do_work(self, t, switch):
        if not switch.get_state():
            return ('IDLE', 0)
        if t > self.my_departure:
            return ('HEATEXT', 0)
        return (self.name, 120)

class HeatExtState:
    name = 'HEATEXT'
    def __init__(self):
        self.entered = False
        self.exit_time = None

    def enter(self):
        self.entered = True

    def do_work(self, t, switch):
        if self.entered:
            self.entered = False
            self.exit_time = t + datetime.timedelta(minutes=60)

        if not switch.get_state():
            return ('IDLE', 0)
        if t > self.exit_time or switch.get_power() < 10:
            switch.set_state(False)
            return ('IDLE', 0)
        return (self.name, 120)


# start here
now = datetime.datetime.now()

t0 = 0

print (now + datetime.timedelta(minutes=heat_time(t0)))

states = { IdleState.name: IdleState(),
           HeatState.name: HeatState(),
           HeatExtState.name: HeatExtState() }

current_state = states[IdleState.name]
current_state.enter()
sw = switch.SwitchTpLink()
schedule = ScheduleDaily()

while True:
    t = current_time()
    (next_state, wait_time) = current_state.do_work(t, sw)

    print(t, current_state.name)

    if (next_state != current_state.name):
        current_state = states[next_state]
        current_state.enter()

    wait(wait_time)
