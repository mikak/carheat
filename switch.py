# -*- coding: utf-8 -*-
"""
Created on Sat Jan 14 10:43:52 2017

@author: mikak
"""

# symbolic link to tplink-smartplug.py
import tplink
import json

class Switch:
    def get_state(self):
        return True
    def set_state(self, s):
        return self.get_state()
    def get_power(self):
        return 500


class SwitchTpLink:
    def __init__(self):
        self.ip = '192.168.1.38'
        self.relay = False
        self.power = 0

    def get_state(self):
        rsp = tplink.send_command(tplink.commands['info'], self.ip)
        if rsp is not None:
            # handle error
            info = json.loads(rsp)
            self.relay = (info['system']['get_sysinfo']['relay_state'] == 1)
        return self.relay
    def set_state(self, s):
        cmd_key = 'on' if s else 'off'
        tplink.send_command(tplink.commands[cmd_key], self.ip)
        return self.get_state()
    def get_power(self):
        rsp = tplink.send_command('{"emeter":{"get_realtime":{}}}', self.ip)
        if rsp is not None:
            # handle error
            info = json.loads(rsp)
            self.power = info['emeter']['get_realtime']['power']
        return self.power
