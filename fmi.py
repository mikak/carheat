# -*- coding: utf-8 -*-
"""
Created on Sat Nov 12 17:18:13 2016

@author: mikak
"""

from owslib.wfs import WebFeatureService
from datetime import datetime, timedelta
import xml.etree.ElementTree as ET

fmi_apikey = 'e0515217-8ad8-4c90-9786-bb4ea605c365'
wfs_url = 'http://data.fmi.fi/fmi-apikey/%s/wfs' % fmi_apikey
sq_id = 'fmi::observations::weather::simple'

ns = {'gis': 'http://www.opengis.net/wfs/2.0',
      'fmi': 'http://xml.fmi.fi/schema/wfs/2.0',
      'gml': 'http://www.opengis.net/gml/3.2'
}

def last_temperatures(location, n=3):
    wfs = WebFeatureService(url=wfs_url, version='2.0.0')

    t1 = datetime.utcnow()
    t0 = t1 - timedelta(hours=1)
    timeformat = '%Y-%m-%dT%H:%MZ'

    r = wfs.getfeature(storedQueryID=sq_id,
                       storedQueryParams={
                           'place': location,
                           'parameters': 't2m',
                           'starttime': t0.strftime(timeformat),
                           'endtime': t1.strftime(timeformat),
                           'maxlocations': n})

    # wfs:member-BsWfsElement-Location(-Point-pos),Time,ParameterName,ParameterValue
    root = ET.fromstring(r.read())
    elems = root.findall('./gis:member/fmi:BsWfsElement/*', ns)

    temperatures = {}
    for x in elems:
        if x.tag.endswith('Location'):
            location = x.find('./gml:Point/gml:pos', ns).text.strip()
        elif x.tag.endswith('ParameterValue'):
            temperatures[location] = x.text
        #print(x.tag, x.text)
    return temperatures

#print(last_temperatures('Liminka'))
