# -*- coding: utf-8 -*-
"""
Created on Sat Jan 14 09:44:03 2017

@author: mikak
"""

import datetime
import fmi
from numpy import average

class Weather:
    def get_temperature(self):
        return -10.0


class WeatherFmi:
    def __init__(self):
        self.last_query = None
        self.last_temperature = None

    def query_expired(self):
        return datetime.datetime.now() - self.last_query > datetime.timedelta(minutes=5)

    def query_temperature(self):
        try:
            data = fmi.last_temperatures('Liminka')
        except:
            data = {}
        if len(data) > 0:
            self.last_temperature = average([ float(d) for d in data.values() ])
        print('QUERY done', self.last_temperature)
        self.last_query = datetime.datetime.now()

    def get_temperature(self):
        if self.last_temperature is None or self.query_expired():
            self.query_temperature()
        return self.last_temperature
